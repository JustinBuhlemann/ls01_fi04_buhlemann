
public class Aufgabe5 {

	public static void main(String[] args) {
		int zahl = 99;
		System.out.println(zahlZuText(zahl));
	}
	
	//hier soll Ihr Quelltext hin

	public static String zahlZuText(int zahl) {
		
		if(zahl > 99 || zahl < 0) {
			return "?";
		}
		
		String returnString = "";
		
		if(zahl < 10) {
			returnString = getZeroToNine(Integer.toString(zahl).toCharArray()[0]);
		}
		if(zahl == 11) {
			returnString = "elf";
		}
		if(zahl == 12) {
			returnString = "zw�lf";
		}
		if(zahl == 16) {
			returnString = "sechzehn";
		}
		if(zahl == 17) {
			returnString = "siebzehn";
		}
		
		if(zahl > 12 && zahl < 20) {
			returnString = getZeroToNine(Integer.toString(zahl).toCharArray()[1]) + "zehn";
		}
		if(zahl >= 20 && zahl < 30) {
			if(Integer.toString(zahl).toCharArray()[1] != '0') {
				returnString += getZeroToNine(Integer.toString(zahl).toCharArray()[1]);
				returnString += "und";
			}
			returnString += "zwanzig";
		}
		if(zahl >= 30 && zahl < 40) {
			if(Integer.toString(zahl).toCharArray()[1] != '0') {
				returnString += getZeroToNine(Integer.toString(zahl).toCharArray()[1]);
				returnString += "und";
			}
			returnString += "drei�ig";
		}
		if(zahl >= 40 && zahl < 50) {
			if(Integer.toString(zahl).toCharArray()[1] != '0') {
				returnString += getZeroToNine(Integer.toString(zahl).toCharArray()[1]);
				returnString += "und";
			}
			returnString += "vierzig";
		}
		if(zahl >= 50 && zahl < 60) {
			if(Integer.toString(zahl).toCharArray()[1] != '0') {
				returnString += getZeroToNine(Integer.toString(zahl).toCharArray()[1]);
				returnString += "und";
			}
			returnString += "f�nfzig";
		}
		if(zahl >= 60 && zahl < 70) {
			if(Integer.toString(zahl).toCharArray()[1] != '0') {
				returnString += getZeroToNine(Integer.toString(zahl).toCharArray()[1]);
				returnString += "und";
			}
			returnString += "sechzig";
		}
		if(zahl >= 70 && zahl < 80) {
			if(Integer.toString(zahl).toCharArray()[1] != '0') {
				returnString += getZeroToNine(Integer.toString(zahl).toCharArray()[1]);
				returnString += "und";
			}
			returnString += "siebzig";
		}
		if(zahl >= 80 && zahl < 90) {
			if(Integer.toString(zahl).toCharArray()[1] != '0') {
				returnString += getZeroToNine(Integer.toString(zahl).toCharArray()[1]);
				returnString += "und";
			}
			returnString += "achtzig";
		}
		if(zahl >= 90 && zahl < 100) {
			if(Integer.toString(zahl).toCharArray()[1] != '0') {
				returnString += getZeroToNine(Integer.toString(zahl).toCharArray()[1]);
				returnString += "und";
			}
			returnString += "neunzig";
		}
		
		return returnString;
	}
	
	public static String getZeroToNine(char zahlChar) {
		switch (zahlChar) {
		case '0':
			return "null";
		case '1':
			return "eins";
		case '2':
			return "zwei";
		case '3':
			return "drei";
		case '4':
			return "vier";
		case '5':
			return "f�nf";
		case '6':
			return "sechs";
		case '7':
			return "sieben";
		case '8':
			return "acht";
		case '9':
			return "neun";
		default:
			return "";
		}
	}
		
}
