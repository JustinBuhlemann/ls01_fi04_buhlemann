
public class Aufgabe4 {
	
	public static void main(String[] args) {
		int zahl = 5;
		System.out.print(romanNumerals(zahl));
	}
	
	//hier soll Ihr Quelltext hin
	public static String romanNumerals(int zahl) {
		String romanNumeral = "";
		
		switch(zahl) {
			case 1:
				romanNumeral = "I";
				break;
			case 5:
				romanNumeral = "V";
				break;
			case 10:
				romanNumeral = "X";
				break;
			case 50:
				romanNumeral = "L";
				break;
			case 100:
				romanNumeral = "C";
				break;
			case 500:
				romanNumeral = "D";
				break;
			case 1000:
				romanNumeral = "M";
				break;
			default:
				romanNumeral = "?";
				break;
		}
		
		return romanNumeral;
	}
}
