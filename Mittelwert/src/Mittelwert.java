import java.util.Scanner;

public class Mittelwert {

   static double x;
   static double y;
   static double m;
	
   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte festlegen:
      // ===========================
	  Scanner scan = new Scanner(System.in); 
	  
	  System.out.println("Geben sie die Anzahl der Werte ein:");
	  int anzahl = scan.nextInt();
	  double addierterWert = 0;
	  double mittelwert = 0;
	  
	  for(int i = 0; i < anzahl; i++) {
		  System.out.println("Geben sie einen Wert an:");
		  addierterWert += scan.nextDouble();
	  }
	   
	   
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      mittelwert = Verarbeitung(anzahl, addierterWert);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      Ausgabe(mittelwert);
   }
   
   public static double Verarbeitung(int anzahl, double addierterWert) {
	   return addierterWert / anzahl;
   }
   
   public static void Ausgabe(double mittelwert) {
	   System.out.printf("Der Mittelwert ist %.2f\n", mittelwert);
   }
}
