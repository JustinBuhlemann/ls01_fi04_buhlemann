package �bungen;

import java.util.Scanner;

public class �bungen {

	public static void main(String[] args) {
		//Z�hlen();
		//Summe();
		//Modulo();
		//Folgen();
		//EinMalEins();
		//Sterne();
		//Treppenstufen();
		Temperaturumrechnung();
	}
	
	public static void Z�hlen() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein:");
		int anzahl = scan.nextInt();
		for(int i = 1; i<=anzahl;i++) {
			System.out.println(i);
		}
	}
	
	public static void Summe() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein:");
		int anzahl = scan.nextInt();
		int wert = 0;
		for(int i = 1; i<=anzahl;i++) {
			wert += i;
			System.out.println(wert);
		}
	}
	
	public static void Modulo() {
		int wert = 0;
		for(int i = 1; i<=200;i++) {
			if(i % 7 == 0) {
				System.out.println("Durch 7 teilbar: " + i);
			}
			if(i % 4 == 0 && i % 5 != 0) {
				System.out.println("Durch 4 teilbar, aber nicht durch 5: " + i);
			}
		}
	}
	
	public static void Folgen() {
		int wert = 0;
		for(int i = 99; i>=9;i-=3) {
			System.out.println(i);
		}
	}
	
	public static void EinMalEins() {
		for(int i = 1; i<=10;i++) {
			for(int j = 1; j<=10;j++) {
				System.out.println(i + "x" + j + ": " + i*j);
			}
		}
	}
	
	public static void Sterne() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein:");
		String sterne = "";
		int anzahl = scan.nextInt();
		for(int i = 1; i<=anzahl;i++) {
			sterne += "*";
			System.out.println(sterne);
		}
	}
	
	public static void Treppenstufen() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Breite ein:");
		int breite = scan.nextInt();
		System.out.println("Geben Sie eine H�he ein:");
		int h�he = scan.nextInt();
		String sterne = "";
		
		for(int i = 1; i<=h�he;i++) {
			for(int j = 1; j<=breite;j++) {
				sterne += "*";
			}
			System.out.println(sterne);
		}
	}
	
	public static void Temperaturumrechnung() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Startwert Celsius:");
		int start = scan.nextInt();
		System.out.println("Endwert Celsius:");
		int ende = scan.nextInt();
		System.out.println("Schrittweite Celsius:");
		int schrittweite = scan.nextInt();
		
		for(int i = start; i<=ende;i+=schrittweite) {
			System.out.println(i + "�C        " + ((i * 9/5) + 32) + "�F");
		}
	}
}
