﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double rückgabebetrag;
       double eingezahlterGesamtbetrag;

       String[] fahrkartenBezeichnung = {
				"Einzelfahrschein Berlin AB",
				"Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC",
				"Kurzstrecke",
				"Tageskarte Berlin AB",
				"Tageskarte Berlin BC",
				"Tageskarte Berlin ABC",
				"Kleingruppen-Tageskarte Berlin AB",
				"Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC",
		};
		
		double[] fahrkartenPreis = {
				2.90,
				3.30,
				3.60,
				1.90,
				8.60,
				9.00,
				9.60,
				23.50,
				24.30,
				24.90,
		};
       
       zuZahlenderBetrag = fahrkartenbestellungErfassenÜbersicht(tastatur, fahrkartenBezeichnung, fahrkartenPreis);
       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       rueckgeldAusgeben(rückgabebetrag);
       
    }
    
    public static double fahrkartenbestellungErfassenÜbersicht(Scanner tastatur, String[] fahrkartenBezeichnung, double[] fahrkartenPreis) {
    	printSelections(fahrkartenBezeichnung, fahrkartenPreis);

    	return ticketNummerErfassen(tastatur, fahrkartenBezeichnung, fahrkartenPreis);
    }
    
    public static void printSelections(String[] fahrkartenBezeichnung, double[] fahrkartenPreis) {
    	for(int i = 0; i < fahrkartenBezeichnung.length; i++) {
    		System.out.println(fahrkartenBezeichnung[i] + " [" + Double.toString(fahrkartenPreis[i]) + "]" + " (" + Integer.toString(i+1) + ")");
    	}
    }
    
    public static double ticketNummerErfassen(Scanner tastatur, String[] fahrkartenBezeichnung, double[] fahrkartenPreis) {
    	System.out.println("Ihre Wahl:");
    	int ticketNummer = tastatur.nextInt();
    	int selectionIndex = -1;
    	
    	
    	for(int i = 0; i < fahrkartenBezeichnung.length; i++) {
    		if(ticketNummer-1 == i) {
    			selectionIndex = i;
    		}
    	}
    	
    	if(selectionIndex == -1) {
    		System.out.println(">>falsche Eingabe<<");
    		return ticketNummerErfassen(tastatur, fahrkartenBezeichnung, fahrkartenPreis);
    	}
    	
    	System.out.println("Anzahl Tickets:");
    	int ticketAnzahl = tastatur.nextInt();
    	
    	for(int i = 0; i < fahrkartenPreis.length; i++) {
    		if(i == selectionIndex) {
    			return fahrkartenPreis[i] * ticketAnzahl;
    		}
    	}
    	return 0;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.println("Noch zu zahlen: " + String.format("%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag)));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    } 
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%.2f", rückgabebetrag) + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
        
        System.out.println("--------------------------------------");
        System.out.println("Neues Ticket:");
        main(null);
    }
}